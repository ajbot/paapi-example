This is a simple example of a python script that will query the Amazon Product Data API based on UPC or ASIN input. 

[View the blog post here](https://blog.ajbothe.com/querying-the-amazon-product-data-api-using-python) 