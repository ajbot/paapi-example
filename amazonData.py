from amazonCred import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG
from amazon.paapi import AmazonAPI
import sys

amazon = AmazonAPI(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG, 'US')

try:
		id = sys.argv[1]
		print ('Querying for item ' + id)
except:
		print('ERROR: ASIN argument required. USAGE: >python amazonData.py [ABCD1234|012345678910')
		sys.exit(1)

if len(id) <= 10:
    isAsin = True
elif 11 <= len(id) <= 13:
    isAsin = False
else:
    print('Invalid entry')
    sys.exit(1)

if isAsin:
    product = amazon.get_product(id)
else:
    product = amazon.search_products(item_count=1, keywords=id)[0]

print("ASIN: %s, UPC: %s" % (product.asin,product.info.external_ids.upc[0]))
print("Title: %s is $%s" % (product.title, product.prices.price.value))